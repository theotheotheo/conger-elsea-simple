<?php


// Remove this in production!
ini_set("display_errors", 1);
error_reporting(E_ALL ^ E_NOTICE);


// Config for folders;
$config['BASE_DIR'] = '/var/www/conger-elsea-simple';
$config['LIB_DIR'] = '/var/www/lib';
require_once($config['LIB_DIR'] . '/smarty/Smarty.class.php');
$smarty = new Smarty();

$smarty->template_dir = $config['BASE_DIR'] . '/tpl';
$smarty->compile_dir  = $config['BASE_DIR'] . '/tpl_c';
$smarty->setCacheDir($config['BASE_DIR'].'/cache');

$smarty->caching = 0;
$smarty->force_compile = true;


// htmlpurifier
require($config['LIB_DIR'].'/htmlpurifier/library/HTMLPurifier.includes.php');
$html_purifier_config = HTMLPurifier_Config::createDefault();
$html_purifier_config->set('Core.Encoding', "UTF-8"); // not using UTF-8
$html_purifier_config->set('HTML.AllowedAttributes', array('a.target', 'a.href', 'img'));
$html_purifier_config->set('Attr.AllowedFrameTargets', array('_blank'));
$purifier = new HTMLPurifier($html_purifier_config);


// Database config + Innitialize ADODB;;
require $config['LIB_DIR'] . '/adodb5/adodb-errorhandler.inc.php'; //adodbe error-handler
require $config['LIB_DIR'] . '/adodb5/adodb.inc.php';
$db_connection = NewADOConnection('mysqli');
$db_connection->Connect('localhost','root','T-Rad1','auvenit_test');



$smarty->assign('base_url', $config['BASE_URL']);
$smarty->assign("root_url", $config['BASE_URL']);
$root_url = $config['BASE_URL'];

/**
	Magic Numbers
*/

// Comments Magic Numbers
$config['CT__MIN_NAME_LENGTH'] = 3;




/**
	Load Classes
*/
