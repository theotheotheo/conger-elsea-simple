<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 13:16:10
         compiled from "/var/www/conger-elsea-simple/tpl/relationships/relationships_federal-state-international_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:115308694456fe4a6ac24335-06422101%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '391d0fb2de9fbb61cf06cb81a9af1b749d4ce10b' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/relationships/relationships_federal-state-international_content.tpl',
      1 => 1459504056,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '115308694456fe4a6ac24335-06422101',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe4a6ac283b3_67894691',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe4a6ac283b3_67894691')) {function content_56fe4a6ac283b3_67894691($_smarty_tpl) {?><div class="the_page_title">RELATIONSHIPS - GOVERNMENT - FEDERAL, STATE, INTERNATIONAL</div>
<p class="general_paragraph"><strong>Federal Government</strong> relationships are more crucial than ever; which/who provide aggressive investigation, enforcement and a calculated budgetary process.  To strengthen that relationship, we are charged to be more efficient, more knowledgeable and more aware.  Conger-Elsea will help you engage the process and navigate a positive result. </p>
<div class="row fed_state_int_row">	
	<div class="col-md-4 col-sm-4">
		<div class="relationships_sprite federal"></div>
	</div>
	<div class="col-md-8 col-sm-8">		
		<div class="row fed_state_int">
			<div class="col-md-6 col-sm-6">
				<ul class="general_ul">
					<li class="general_paragraph">Department of Transportation</li>
					<li class="general_paragraph">Pipeline and Hazardous Materials<br />Safety Administration</li>
					<li class="general_paragraph">Department of Interior</li>
					<li class="general_paragraph">Bureau of Safety and<br />Environmental Enforcement</li>
					<li class="general_paragraph">EPA</li>
					<li class="general_paragraph">National Science Foundation</li>
					<li class="general_paragraph">U.S. Nuclear Regulatory<br />Commission</li>
				</ul>
			</div>
			<div class="col-md-6 col-sm-6">
				<ul class="general_ul">
					<li class="general_paragraph">U.S. Navy</li>
					<li class="general_paragraph">NASA</li>
					<li class="general_paragraph">Homeland Security</li>
					<li class="general_paragraph">OSHA</li>
					<li class="general_paragraph">TVA</li>
					<li class="general_paragraph">Chemical Safety and Hazard<br />Investigation Board</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<hr class="custom_hr">
<p class="general_paragraph"><strong>State Government</strong> agencies have very real challenges and responsibilities to your citizens, showing research, thorough investigation and a solution that is wise in knowledge, experience and value for you and your state team.  With you, Conger-Elsea will listen and together we will mitigate risk, enhance the safety culture and improve performance for the people and businesses of your state.</p>
<div class="row fed_state_int_row">	
	<div class="col-md-4 col-sm-4">
		<div class="relationships_sprite state"></div>
	</div>
	<div class="col-md-8 col-sm-8">		
		<div class="row fed_state_int">
			<div class="col-md-6 col-sm-6">
				<ul class="general_ul">
					<li class="general_paragraph">Public Service Commission</li>
					<li class="general_paragraph">Public Utilities Commission</li>
					<li class="general_paragraph">Fire Marshalls</li>
				</ul>
			</div>
			<div class="col-md-6 col-sm-6">
				<ul class="general_ul">
					<li class="general_paragraph">Railroad Commission</li>
					<li class="general_paragraph">Rescue/Disaster/Safety Services</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<hr class="custom_hr">
<p class="general_paragraph"><strong>International Government</strong> relationships require a seasoned partner who you can trust and expect results.  Conger-Elsea IS that partner.  We’re passionate about our space, and passionate about qualified support and assistance for YOU and your government, no matter where you are, around the world. </p>
<div class="row fed_state_int_row">	
	<div class="col-md-4 col-sm-4">
		<div class="relationships_sprite international"></div>
	</div>
	<div class="col-md-8 col-sm-8">		
		<div class="row fed_state_int">
			<div class="col-md-6 col-sm-6">
				<ul class="general_ul">
					<li class="general_paragraph">Canadian Nuclear Safety Commission</li>
					<li class="general_paragraph">International Atomic Energy Agency</li>
					<li class="general_paragraph">European Union</li>
					<li class="general_paragraph">Nuclear Regulators of Ukraine</li>
					<li class="general_paragraph">Russia</li>
					<li class="general_paragraph">Spain</li>
					<li class="general_paragraph">France</li>
					<li class="general_paragraph">Lithuania</li>
					<li class="general_paragraph">Bulgaria</li>
				</ul>
			</div>
			<div class="col-md-6 col-sm-6">
				<ul class="general_ul">
					<li class="general_paragraph">India</li>
					<li class="general_paragraph">Pakistan</li>
					<li class="general_paragraph">Hungary</li>
					<li class="general_paragraph">Czech Republic</li>
					<li class="general_paragraph">Slovakia</li>
					<li class="general_paragraph">Croatia</li>
					<li class="general_paragraph">Finland</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<hr class="custom_hr">
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p><?php }} ?>
