<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 12:55:00
         compiled from "/var/www/conger-elsea-simple/tpl/services/services_training_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:188048043256fe4574e8e4d2-61030384%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5ab9aabebfb166ea7935675ae1aba2b57594eb0e' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/services/services_training_content.tpl',
      1 => 1459453880,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '188048043256fe4574e8e4d2-61030384',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe4574e91125_93343756',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe4574e91125_93343756')) {function content_56fe4574e91125_93343756($_smarty_tpl) {?><div class="the_page_title">SERVICES - TRAINING</div>
<p class="general_paragraph">More than 18,000 industry professionals have successfully completed the Conger- Elsea training curriculum of their systematic approach to root cause analysis and corrective action program assessment. The Conger-Elsea process for investigation and root cause analysis has been presented to the highest level investigation personnel in both government and the private sector. With clients that include: the US Navy, the U.S. Nuclear Regulatory Commission, the CSB, the EPA, PHMSA, Public Service Commissions, and a host of high-level engineering firms. They have all partnered with our experience to add value, mitigate risk, and enhance a safety culture for their people and their organizations.<br /><br />Conger-Elsea also provides specialized training in the areas of human performance improvement, interpersonal skills improvement, safety observation, behavior-based safety, and management assessments. All of the training is carefully crafted to match your needs …Not a one-size-fits-all approach for YOUR training solutions. The Conger-Elsea team has always used a “hands-on” learning approach with realistic practice, real world issues, and relevant content.</p>
<div class="row services_training">
	<div class="col-md-6 col-sm-6">
		<ul class="general_ul">
			<li class="general_paragraph">Root Cause Analysis / Incident Investigation</li>
			<li class="general_paragraph">Management Oversight and Risk Tree (MORT)</li>
			<li class="general_paragraph">Corrective Action</li>
		</ul>
	</div>
	<div class="col-md-6 col-sm-6">
		<ul class="general_ul">
			<li class="general_paragraph">Safety / Safety Culture Training</li>
			<li class="general_paragraph">Human Performence Improvement</li>
			<li class="general_paragraph">Regulatory Communication, Compliance and Education</li>
		</ul>
	</div>
</div>
<p class="bottom_normal_blue_paragraph">Conger Elsea … Managing Risk.  Enhancing Culture.  Improving Performance.</p>
<div class="hellier_container">
	<div class="hellier_logo">
		<img src="<?php echo '<?php'; ?>
 echo $_smarty_tpl->tpl_vars['logos_path']->value;<?php echo '?>'; ?>

hellier_logo.png" alt="hellier_logo"/>
	</div>
	<div class="hellier_description">
		<div class="maroon_small_top">NONDESTRUCTIVE TESTING</div>
		<div class="big_black_bottom">NDT TRAINING AND CONSULTING</div>
	</div>
	<div class="hellier_button"><a href="#">VISIT HELLIER</a></div>
</div>
<?php }} ?>
