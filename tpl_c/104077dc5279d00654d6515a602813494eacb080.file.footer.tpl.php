<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 14:43:34
         compiled from "/var/www/conger-elsea-simple/tpl/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:158553843156fe5ee68bf9b5-73047297%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '104077dc5279d00654d6515a602813494eacb080' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/footer.tpl',
      1 => 1458918645,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '158553843156fe5ee68bf9b5-73047297',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'logos_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe5ee68c3c85_75688401',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe5ee68c3c85_75688401')) {function content_56fe5ee68c3c85_75688401($_smarty_tpl) {?><footer>	
	<div class="solution_partner">
		<div class="white_main_logo">
			<img src="<?php echo $_smarty_tpl->tpl_vars['logos_path']->value;?>
white_main_logo.png" alt="white_main_logo"/>
		</div>
		<div class="newsleter_text">
			<div class="newsletter_big">Join our newsletter.<span class="newsletter_small">Get exclusive content, tips, and special offers.</span></div>			
		</div>
		<div class="join_newsletter_button"><a href="#">JOIN NOW</a></div>
	</div>
	<div class="contact_data">
		<ul class="contact_data_ul">
			<li class="contact_data_li">
				<div class="li_title">Find:</div>
				<div class="li_main_content">3440 Blue Springs Rd., Ste. 102<br />Kennesaw, GA, 30144</div>
			</li>
			<li class="contact_data_li">
				<div class="li_title">Call:</div>
				<div class="li_main_content"><a href="tel:8008758709">Toll free: 800-875-8709</a><br /><a href="Voice:7709261131">Voice: 770-926-1131</a></div>
			</li>
			<li class="contact_data_li">
				<div class="li_title">Email:</div>
				<div class="li_main_content"><a href="mailto:info@conger-elsea.com">info@conger-elsea.com</a></div>
			</li>
			<li class="contact_data_li">
				<div class="li_title">Follow Us:</div>
				<div class="li_main_content"><a href="#"><img class="linkedin_logo" src="<?php echo $_smarty_tpl->tpl_vars['logos_path']->value;?>
linkedin_logo.png"/></a></div>
			</li>
			<li class="contact_data_li">
				<div class="li_title">View Our Home Website:</div>
				<div class="li_main_content">
					<img class="white_advantae_logo" src="<?php echo $_smarty_tpl->tpl_vars['logos_path']->value;?>
white_advantage_logo.png" alt="white_advantae_logo"/>
				</div>
			</li>
		</ul>
	</div>	
	<div class="footer_last_row">
		<div class="main_menu footer">
			<a href="#" class="menu_item">ABOUT US</a>
			<a href="#" class="menu_item">SERVICES</a>
			<a href="#" class="menu_item">MARKETS</a>
			<a href="#" class="menu_item">RELATIONSHIPS</a>
			<a href="#" class="menu_item">RESOURCES</a>
			<a href="#" class="menu_item">CAREERS</a>
			<a href="#" class="menu_item">CONTACT US</a>
		</div>
		<div class="credits">Website Developed and Hosted by Digital Lagoon</div>
	</div>
</footer><?php }} ?>
