<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-30 13:48:37
         compiled from "/var/www/conger-elsea-simple/tpl/body-main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:149119888756fbaf05535144-73180464%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d9b8be47662c79b0e9f7d961f888da82afe1a42' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/body-main.tpl',
      1 => 1459329257,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '149119888756fbaf05535144-73180464',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'logos_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fbaf05540971_95414201',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fbaf05540971_95414201')) {function content_56fbaf05540971_95414201($_smarty_tpl) {?>	<body>
		<div class="gradient_top"></div>
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('slider.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="row first_body_row">
			<div class="col-md-6 col-sm-6 col-xs-6 clear-col_980">
				<div class="training_container sprite training_pos">
					<div class="training_button">
						<a href="#">TRAINING</a>
					</div>
				</div>	
				<div class="training_button_small_res">
					<a href="#">TRAINING</a>
				</div>			
			</div>			
			<div class="col-md-6 col-sm-6 col-xs-6 clear-col_980">
				<div class="consulting_container sprite consulting_pos">
					<div class="consulting_button">
						<a href="#">CONSULTING</a>
					</div>
				</div>
				<div class="consulting_button_small_res">
					<a href="#">CONSULTING</a>
				</div>	
			</div>				
		</div>
		<div class="white_background_container">
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-8 clear-col_800">
					<p class="general_paragraph"> For more than three decades Conger-Elsea is recognized as the trusted global leader for premier, go-to TRAINING and CONSULTING solutions and services for: Root Cause Analysis/Incident Investigation, Corrective Action, Safety/Safety Culture Training, Human Performance Improvement, Regulatory Compliance and Education. With a specialized team of industry-focused professionals, Conger-Elsea develops a rich understanding of each clients business or project and then applies insight, skills and the resources required to address relevant, industry-specific issues and opportunities.</p>
					<p class="bottom_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 clear-col_800">
					<div class="right_panel_post">
						<p class="post_text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ullamcorper est et neque tempus, et suscipit eros cursus. Integer quis leo est. Duis arcu odio, suscipit vitae diam eget, efficitur gravida lacus.”</p>
						<p class="post_author">- John Doe, Job Title<br />Name of Company</p>
					</div>
				</div>
			</div>
			<hr class="custom_hr">
			<div class="row" id="training_row">
				<div class="top_indicator">TRAINING</div>
				<div class="col-md-9 col-sm-9 col-xs-9 clear-col_800">
					<div class="see_at_all_res">
						<p class="general_paragraph">The Conger-Elsea training curricula has been presented to more than 18,000 industry professionals who have been certified in the use of this invaluable tool for:</p>
						<ul class="general_ul">
							<li class="general_paragraph">Root Cause Analysis / Incident Investigation</li>
							<li class="general_paragraph">Management Oversight and Risk Tree (MORT)</li>
						</ul>
					</div>
					<a href="#training_row" class="read_more_res">Read more</a>
					<div class="see_at_read_more">
						<p class="general_paragraph">Conger-Elsea also provide specialized training in the areas of:</p>
						<ul class="general_ul">
							<li class="general_paragraph">Safety/Safety Culture Training</li>
							<li class="general_paragraph">Human Performance Improvement</li>
							<li class="general_paragraph">Regulatory Communication, Compliance and Education</li>
						</ul>
						<p class="general_paragraph">C-E has established strategic relationships with a host of U.S. and international clients where the truth, accuracy and fact-based analyses are critical and essential.  The training workshop experience is carefully crafted to match your needs …Not a one-size-fits-all approach for YOUR training solutions.  The Conger-Elsea team tailors each engagement to meet and exceed the clients’ expectations in managing risks, enhancing culture and improving performance.</p>
						<a href="#training_row_closed" class="read_less_res">Read less</a>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3 clear-col_800">
					<div class="sprite post_picture post_pic_training">
						<div class="overlay_colour"></div>
						<div class="overlay_text">TRAINING</div>
					</div>
				</div>
			</div>
			<div class="hellier_container">
				<div class="hellier_logo">
					<img src="<?php echo $_smarty_tpl->tpl_vars['logos_path']->value;?>
hellier_logo.png" alt="hellier_logo"/>
				</div>
				<div class="hellier_description">
					<div class="maroon_small_top">NONDESTRUCTIVE TESTING</div>
					<div class="big_black_bottom">NDT TRAINING AND CONSULTING</div>
				</div>
				<div class="hellier_button"><a href="#">VISIT HELLIER</a></div>
			</div>
			<div class="row" id="consulting_row">
				<div class="top_indicator"><span class="consulting_indicator">CONSULTING</span></div>
				<div class="col-md-9 col-sm-9 col-xs-9 clear-col_800">
					<div class="see_at_all_res">
						<p class="general_paragraph">dentifying, analyzing, evaluating and advising on matters of risk takes many forms.  It’s a refined process of experience, insight, skills and the resources required to address YOUR specific issues. To name a few, Conger-Elsea consulting activities have included:  </p>
					</div>
					<a href="#consulting_row" class="read_more_res">Read more</a>
					<div class="see_at_read_more">
						<ul class="general_ul">
							<li class="general_paragraph">Root Cause Analysis/Incident Investigation</li>
							<li class="general_paragraph">Corrective Action/Development/Improvement</li>
							<li class="general_paragraph">Safety/Safety Culture Assessment</li>
							<li class="general_paragraph">Audits and Evaluations</li>
							<li class="general_paragraph">Nuclear Material Handling</li>
							<li class="general_paragraph">Nondestructive Testing - NDT</li>
							<li class="general_paragraph">Nuclear Licensing</li>
							<li class="general_paragraph">Strategic Regulatory Counsel</li>
							<li class="general_paragraph">Significance Determination Process</li>
							<li class="general_paragraph">Preparation and Support for Enforcement & Regulatory Conferences</li>
							<li class="general_paragraph">Preparation and Support for ALL NRC Inspections<br /> Including: 95001, 95002 and 95003 </li>
						</ul>
						<a href="#consulting_row_closed" class="read_less_res">Read less</a>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3 clear-col_800">
					<div class="sprite post_picture post_pic_consulting">
						<div class="overlay_colour"></div>
						<div class="overlay_text">CONSULTING</div>
					</div>
				</div>
			</div>
		</div>
		<hr class="custom_hr after_white_container">
		<div class="row advantage_opportunities_section">
			<div class="col-md-6 col-sm-6 col-xs-6 clear-col_930">
				<div class="advantage_title j_title">CONGER-ELSEA ADVANTAGE</div>
				<div class="row j_row">
					<div class="col-md-6 col-sm-6 col-xs-6 clear_both_580">
						<div class="advantage_pic_border">
							<div class="sprite advantage_pic advantage_pic_pos"></div>
						</div>
						<div class="logo_advantage">
							<img src="<?php echo $_smarty_tpl->tpl_vars['logos_path']->value;?>
advantage_logo.png" alt="advantage_logo"/>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 clear_both_580">
						<div class="advantage_small_title">Headline Text Here or If Longer<br />Will Wrap</div>
						<div class="advantage_excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.<br /><br />Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</div>
					</div>
				</div>
				<div class="find_classes_link"><a href="#">FIND CLASSES NOW</a></div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6 clear-col_930">
				<div class="opportunities_title j_title">UPCOMING COURSE OPPORTUNITIES</div>
				<div class="prev_small_res"></div>
				<div class="row j_row opportunities_row">
					<div class="col-md-6 col-sm-6 col-xs-6 width_full_small_res">
						<div class="sprite post_picture post_pic_opportunities">
							<div class="overlay_colour"></div>
							<div class="overlay_text">CORRECTIVE ACTION<br />PROGRAM<br />(CAP)</div>
						</div>
						<div class="short_opportunity_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.....</div>
						<div class="opportunity_link_container"><a href="#">Read More</a></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 width_full_small_res">
						<div class="sprite post_picture post_pic_opportunities">
							<div class="overlay_colour"></div>
							<div class="overlay_text">CORRECTIVE ACTION<br />PROGRAM<br />(CAP)</div>
						</div>
						<div class="short_opportunity_description">Sed facete impedit te, et iriure volutpat adipiscing cum. Fugit malorum cu est, in sit inani appetere appellantur...</div>
						<div class="opportunity_link_container"><a href="#">Read More</a></div>
					</div>
				</div>
				<div class="next_small_res"></div>
				<div class="enroll_opportunity_link"><a href="#">ENROLL NOW</a></div>
			</div>
		</div>
		<hr class="custom_hr after_opp_adv_section">
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="bottom_gradient"></div>
	</body>
</html><?php }} ?>
