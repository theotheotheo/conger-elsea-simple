<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 12:55:00
         compiled from "/var/www/conger-elsea-simple/tpl/services/body_services.tpl" */ ?>
<?php /*%%SmartyHeaderCode:184416435256fe4574e7aab6-32410983%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4efb272a643ec6d715ffd2b16b78def6ec0f9f73' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/services/body_services.tpl',
      1 => 1459454074,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '184416435256fe4574e7aab6-32410983',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'services_view' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe4574e852f1_09339264',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe4574e852f1_09339264')) {function content_56fe4574e852f1_09339264($_smarty_tpl) {?><body class="services_page">
		<div class="gradient_top"></div>
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate ("services/top_side.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="white_background_container">
      <?php if ($_smarty_tpl->tpl_vars['services_view']->value=='training') {?>
        <?php echo $_smarty_tpl->getSubTemplate ("services/services_training_content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['services_view']->value=='consulting') {?>
        <?php echo $_smarty_tpl->getSubTemplate ("services/services_consulting_content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['services_view']->value=='main') {?>
        <?php echo $_smarty_tpl->getSubTemplate ("services/services_main_content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <?php }?>
		</div>
		<hr class="custom_hr after_opp_adv_section">
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="bottom_gradient"></div>
	</body>
</html>
<?php }} ?>
