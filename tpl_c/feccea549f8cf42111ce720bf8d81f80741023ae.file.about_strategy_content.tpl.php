<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 10:22:44
         compiled from "/var/www/conger-elsea-simple/tpl/about/about_strategy_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:193573284256fe21c42ddb78-65331132%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'feccea549f8cf42111ce720bf8d81f80741023ae' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/about/about_strategy_content.tpl',
      1 => 1459452080,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '193573284256fe21c42ddb78-65331132',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe21c42dfaf1_30917100',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe21c42dfaf1_30917100')) {function content_56fe21c42dfaf1_30917100($_smarty_tpl) {?><div class="row">
	<div class="col-md-8 col-sm-8 clear_both_980">
		<div class="the_page_title">ABOUT US - STRATEGY</div>
		<p class="general_paragraph">Conger-Elsea develops “leaders” who guide, support, encourage and enable team members to reach their fullest potential.  We’re not focused purely on oversight.  We strategize, refocus and consult with YOUR team.  Elevating their skill sets from rote compliance to full comprehension of the regulatory trends, industry issues and the approved methods for reducing risk while building a culture of only the highest performance and productivity.</p>
	</div>
	<div class="col-md-4 col-sm-4 clear_both_980">
		<div class="process_strategy_sprite pos_strategy"></div>
	</div>
</div>
<div class="blank_40"></div>
<?php }} ?>
