<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-31 23:02:30
         compiled from "/var/www/conger-elsea-simple/tpl/markets/markets_energy_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:133723687456fd8256c7e7d7-97094973%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '380e5252b11b0910691c268311604b816b7c3a08' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/markets/markets_energy_content.tpl',
      1 => 1459454470,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '133723687456fd8256c7e7d7-97094973',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fd8256c7f6d0_25460787',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fd8256c7f6d0_25460787')) {function content_56fd8256c7f6d0_25460787($_smarty_tpl) {?><div class="row">	
	<div class="col-md-7 col-sm-7">
		<div class="the_page_title">MARKETS - ENERGY</div>
		<p class="general_paragraph">Conger-Elsea provides comprehensive support services and tools for:  Public and Private Power Generation and Distribution, Oil and Natural Gas, Pipeline Management Companies and support for Nuclear Materials Industries.</p>
		<ul class="general_ul">
			<li class="general_paragraph">Power Generation & Distribution</li>
			<li class="general_paragraph">Oil & Natural Gas</li>
			<li class="general_paragraph">Pipeline</li>
			<li class="general_paragraph">Nuclear Materials</li>
		</ul>
	</div>
	<div class="col-md-5 col-sm-5">
		<div class="markets_sprite energy_emblemma"></div>
	</div>
</div>
<?php }} ?>
