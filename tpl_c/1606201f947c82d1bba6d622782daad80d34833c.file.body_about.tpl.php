<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 10:22:44
         compiled from "/var/www/conger-elsea-simple/tpl/about/body_about.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29734199756fe21c42c1437-57111713%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1606201f947c82d1bba6d622782daad80d34833c' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/about/body_about.tpl',
      1 => 1459453426,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29734199756fe21c42c1437-57111713',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'about_view' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe21c42cf386_86874522',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe21c42cf386_86874522')) {function content_56fe21c42cf386_86874522($_smarty_tpl) {?><body class="about_page">
		<div class="gradient_top"></div>
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('about/top_side.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="white_background_container">
			<?php if ($_smarty_tpl->tpl_vars['about_view']->value=="team") {?>
				<?php echo $_smarty_tpl->getSubTemplate ('about/about_team_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['about_view']->value=="strategy") {?>
				<?php echo $_smarty_tpl->getSubTemplate ('about/about_strategy_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['about_view']->value=="process") {?>
				<?php echo $_smarty_tpl->getSubTemplate ('about/about_process_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['about_view']->value=="ethics") {?>
				<?php echo $_smarty_tpl->getSubTemplate ('about/about_ethics_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['about_view']->value=="main") {?>
				<?php echo $_smarty_tpl->getSubTemplate ('about/about_main_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
		</div>
		<hr class="custom_hr after_opp_adv_section">
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="bottom_gradient"></div>
	</body>
</html>
<?php }} ?>
