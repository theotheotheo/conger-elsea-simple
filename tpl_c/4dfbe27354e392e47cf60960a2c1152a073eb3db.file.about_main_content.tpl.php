<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 00:04:24
         compiled from "/var/www/conger-elsea-simple/tpl/about/about_main_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:182285941356fd90d8494f84-44980982%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4dfbe27354e392e47cf60960a2c1152a073eb3db' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/about/about_main_content.tpl',
      1 => 1459452047,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '182285941356fd90d8494f84-44980982',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fd90d8496836_72873553',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fd90d8496836_72873553')) {function content_56fd90d8496836_72873553($_smarty_tpl) {?><div class="the_page_title">ABOUT US</div>
<p class="general_paragraph">Conger-Elsea represents a vast collection of expertise from private businesses and government agencies with an impeccable history of success.  The firm includes over sixty experienced professionals with an in-depth knowledge of: </p>
<ul class="general_ul">
	<li class="general_paragraph">Root Cause Analysis/Incident Investigation,</li>
	<li class="general_paragraph">Corrective Action/Development/Improvement,</li>
	<li class="general_paragraph">Safety/Safety Culture Assessments,</li>
	<li class="general_paragraph">Audits and Evaluations,</li>
	<li class="general_paragraph">Nuclear Material Handling,</li>
	<li class="general_paragraph">Nuclear Licensing, Strategic Regulatory Counsel,</li>
	<li class="general_paragraph">Significance Determination Process,</li>
	<li class="general_paragraph">Preparation and Support for Enforcement & Regulatory Conferences</li>
	<li class="general_paragraph">and Preparation and Support for ALL NRC Inspections. Including:  95001, 95002 and 95003.</li>
</ul>
<p class="general_paragraph">Conger-Elsea also provides the Training Solutions for each, enabling your team to manage these significant performance improvement initiatives.  All aspects of our business are characterized by a legacy of ethics and unmatched expertise from our founders, Dorian Conger and Ken Elsea; whose fingerprints are indelibly fixed to all the firm’s content and services.  When you’re searching for the real solution that will mitigate risk, enhance the safety culture and improve performance … </p>
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
<?php }} ?>
