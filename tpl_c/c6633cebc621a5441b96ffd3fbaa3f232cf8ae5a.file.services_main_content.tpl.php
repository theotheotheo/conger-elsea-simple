<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-31 22:54:31
         compiled from "/var/www/conger-elsea-simple/tpl/services/services_main_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:66325213956fd807710d330-44770903%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c6633cebc621a5441b96ffd3fbaa3f232cf8ae5a' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/services/services_main_content.tpl',
      1 => 1459453871,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '66325213956fd807710d330-44770903',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fd807710e237_09715184',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fd807710e237_09715184')) {function content_56fd807710e237_09715184($_smarty_tpl) {?><div class="row">
	<div class="col-md-8 col-sm-8 clear_both_980">
		<div class="the_page_title">SERVICES</div>
		<p class="general_paragraph">Conger-Elsea  … Provides Training and Consulting Services for Identifying, Analyzing, Evaluating and Advising on Matters of Risk.  Industry leaders from around the world embrace and practice these proven methods to evaluate root causes, assess safety culture, and develop corrective actions for accidents, incidents and regulatory compliance.</p>
	</div>
	<div class="col-md-4 col-sm-4 clear_both_980">
		<div class="services_main_right">
			<img src="../images/services/services_main_right.jpg" alt="services_main_right.jpg"/>
		</div>
	</div>
</div>
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
<?php }} ?>
