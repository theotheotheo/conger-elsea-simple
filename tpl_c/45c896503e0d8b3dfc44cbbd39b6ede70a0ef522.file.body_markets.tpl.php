<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 12:50:18
         compiled from "/var/www/conger-elsea-simple/tpl/markets/body_markets.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82322428756fe445a22cba7-25428540%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '45c896503e0d8b3dfc44cbbd39b6ede70a0ef522' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/markets/body_markets.tpl',
      1 => 1459454541,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '82322428756fe445a22cba7-25428540',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'markets_view' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe445a23aab5_52858809',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe445a23aab5_52858809')) {function content_56fe445a23aab5_52858809($_smarty_tpl) {?><body class="markets_page">
		<div class="gradient_top"></div>
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('markets/top_side.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="white_background_container">
			<?php if ($_smarty_tpl->tpl_vars['markets_view']->value=='energy') {?>
				<?php echo $_smarty_tpl->getSubTemplate ("markets/markets_energy_content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['markets_view']->value=='private-commercial') {?>
				<?php echo $_smarty_tpl->getSubTemplate ("markets/markets_private-commercial_content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['markets_view']->value=='government-regulatory') {?>
				<?php echo $_smarty_tpl->getSubTemplate ("markets/markets_government-regulatory_content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['markets_view']->value=='main') {?>
				<?php echo $_smarty_tpl->getSubTemplate ("markets/markets_main_content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
		</div>
		<hr class="custom_hr after_opp_adv_section">
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="bottom_gradient"></div>
	</body>
</html>
<?php }} ?>
