<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 10:22:26
         compiled from "/var/www/conger-elsea-simple/tpl/about/about_process_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:99704397656fe21b22cf246-97575581%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '079935e9a8cc51ed12aae9df1da72585d8e6837c' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/about/about_process_content.tpl',
      1 => 1459452063,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '99704397656fe21b22cf246-97575581',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe21b22f2427_70259943',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe21b22f2427_70259943')) {function content_56fe21b22f2427_70259943($_smarty_tpl) {?><div class="the_page_title">ABOUT US - PROCESS</div>
<p class="general_paragraph">The Conger-Elsea Process represents over three decades of leadership experience and a vast collection of specialized tools and solutions that have set the bar for excellence.  A standard that is founded on industry experience, superior knowledge, and analytic solutions tools like the Management Oversight and Risk Tree (MORT).  MORT is a system safety solution that has been developed, improved and successfully demonstrated in actual frontline applications, projects and analysis for decades.  Originally developed and created by the Department of Energy, Conger - Elsea has been responsible for adapting, updating and maintaining the MORT copyright process ever since.</p>
<div class="row process_sprites">
	<div class="col-md-4 col-sm-4 col-xs-4">
		<div class="process_strategy_sprite pos_industry"></div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-4">
		<div class="process_strategy_sprite pos_knowledge"></div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-4">
		<div class="process_strategy_sprite pos_solution"></div>
	</div>
</div>
<div class="about_big_blue_title">We strategize, refocus and consult with YOUR team.</div>
<p class="general_paragraph">We help our clients resolve issues while promoting, mentoring and supporting on-the-job interaction.  Our Training Strategy is about imparting wisdom … Above and beyond the fundamentals of  job knowledge, policies and procedures … Conger-Elsea.</p>
<div class="blank_40"></div>
<?php }} ?>
