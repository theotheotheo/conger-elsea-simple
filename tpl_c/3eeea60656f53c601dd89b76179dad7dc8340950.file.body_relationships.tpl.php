<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 13:16:10
         compiled from "/var/www/conger-elsea-simple/tpl/relationships/body_relationships.tpl" */ ?>
<?php /*%%SmartyHeaderCode:168080366256fe4a6ac10478-55515067%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3eeea60656f53c601dd89b76179dad7dc8340950' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/relationships/body_relationships.tpl',
      1 => 1459456142,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168080366256fe4a6ac10478-55515067',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'relationships_view' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe4a6ac1a616_96218828',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe4a6ac1a616_96218828')) {function content_56fe4a6ac1a616_96218828($_smarty_tpl) {?><body class="relationships_page">
		<div class="gradient_top"></div>
		<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('relationships/top_side.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="white_background_container">
			<?php if ($_smarty_tpl->tpl_vars['relationships_view']->value=="federal-state-international") {?>
				<?php echo $_smarty_tpl->getSubTemplate ('relationships/relationships_federal-state-international_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['relationships_view']->value=="testimonials") {?>
				<?php echo $_smarty_tpl->getSubTemplate ('relationships/relationships_testimonials_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['relationships_view']->value=="main") {?>
				<?php echo $_smarty_tpl->getSubTemplate ('relationships/relationships_main_content.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php }?>
		</div>
		<hr class="custom_hr after_opp_adv_section">
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="bottom_gradient"></div>
	</body>
</html>
<?php }} ?>
