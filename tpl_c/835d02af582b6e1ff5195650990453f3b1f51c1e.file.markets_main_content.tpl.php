<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-31 23:02:37
         compiled from "/var/www/conger-elsea-simple/tpl/markets/markets_main_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82973366256fd825db39af0-43455234%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '835d02af582b6e1ff5195650990453f3b1f51c1e' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/markets/markets_main_content.tpl',
      1 => 1459454486,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '82973366256fd825db39af0-43455234',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fd825db3b519_20935223',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fd825db3b519_20935223')) {function content_56fd825db3b519_20935223($_smarty_tpl) {?><div class="the_page_title">MARKETS</div>
<p class="general_paragraph">Conger-Elsea separates market support into three relevant areas. Each strategically enabled to direct Domestic and International services.</p>
<div class="row markets_main">
	<div class="col-md-4 col-sm-4">
		<div class="markets_main_container">
			<div class="markets_sprite energy_pos"></div>
			<div class="markets_main_overlay">
				<div class="markets_overlay_text">ENERGY MARKETS</div>
				<a href="?action=markets&view=energy"></a>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		<div class="markets_main_container">
			<div class="markets_sprite gov_reg_pos"></div>
			<div class="markets_main_overlay">
				<div class="markets_overlay_text">GOVERNMENT REGULATORY MARKETS</div>
				<a href="?action=markets&view=government-regulatory"></a>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		<div class="markets_main_container">
			<div class="markets_sprite priv_comm_pos"></div>
			<div class="markets_main_overlay">
				<div class="markets_overlay_text">PRIVATE COMMERCIAL MARKETS</div>
				<a href="?action=markets&view=private-commercial"></a>
			</div>
		</div>
	</div>
</div>
<p class="general_paragraph">As part of each market we effectively deploy specially designed services and products that will provide real solutions, for you, our client.  We share our resources to build long-term relationships. </p>
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
<?php }} ?>
