<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 10:05:02
         compiled from "/var/www/conger-elsea-simple/tpl/services/services_consulting_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:171835705656fe1d9edfd926-49632159%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '25eb5e58ae3e1cd40bf0dbf4820181a1cddc9de1' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/services/services_consulting_content.tpl',
      1 => 1459453859,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '171835705656fe1d9edfd926-49632159',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fe1d9edfff68_10277908',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fe1d9edfff68_10277908')) {function content_56fe1d9edfff68_10277908($_smarty_tpl) {?><div class="the_page_title">SERVICES - CONSULTING</div>
<p class="general_paragraph">A refined process of experience, insight, skills and the resources required to address YOUR specific issues.  To name a few, Conger-Elsea’s consulting activities include:</p>
<div class="row services_training">
	<div class="col-md-6 col-sm-6">
		<ul class="general_ul">
			<li class="general_paragraph">Root Cause Analysis / Incident Investigation</li>
			<li class="general_paragraph">Corrective Action / Development / Improvemet</li>
			<li class="general_paragraph">Safety / Safety Culture Assessment</li>
			<li class="general_paragraph">Human Performence Improvement</li>
			<li class="general_paragraph">Audits and Evaluations</li>
			<li class="general_paragraph">Nuclear Licensing</li>
		</ul>
	</div>
	<div class="col-md-6 col-sm-6">
		<ul class="general_ul">
			<li class="general_paragraph">Strategic Regulatory Counsel</li>
			<li class="general_paragraph">Preparation and Support for ALL NRC Inspections Including 95001, 95002 and 95003</li>
			<li class="general_paragraph">Segnificance Determination Process</li>
			<li class="general_paragraph">Preparation and Support for Enforcement and Regulatory Conferences</li>
			<li class="general_paragraph">Regulatory Compliance</li>
		</ul>
	</div>
</div>
<p class="bottom_normal_blue_paragraph">Conger Elsea … Managing Risk.  Enhancing Culture.  Improving Performance.</p>
<?php }} ?>
