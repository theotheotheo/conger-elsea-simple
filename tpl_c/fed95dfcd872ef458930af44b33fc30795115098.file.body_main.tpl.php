<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-30 14:58:09
         compiled from "/var/www/conger-elsea-simple/tpl/about/body_main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:72059326856fbbf5131b162-30132489%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fed95dfcd872ef458930af44b33fc30795115098' => 
    array (
      0 => '/var/www/conger-elsea-simple/tpl/about/body_main.tpl',
      1 => 1459337498,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '72059326856fbbf5131b162-30132489',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fbbf5131fdb4_60487124',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fbbf5131fdb4_60487124')) {function content_56fbbf5131fdb4_60487124($_smarty_tpl) {?>	<body>
		<div class="gradient_top"></div>
		<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php echo $_smarty_tpl->getSubTemplate ('about/top_side.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="white_background_container">
			<div class="the_page_title">ABOUT US</div>
			<p class="general_paragraph">Conger-Elsea represents a vast collection of expertise from private businesses and government agencies with an impeccable history of success.  The firm includes over sixty experienced professionals with an in-depth knowledge of: </p>
			<ul class="general_ul">
				<li class="general_paragraph">Root Cause Analysis/Incident Investigation,</li>
				<li class="general_paragraph">Corrective Action/Development/Improvement,</li>
				<li class="general_paragraph">Safety/Safety Culture Assessments,</li>
				<li class="general_paragraph">Audits and Evaluations,</li>
				<li class="general_paragraph">Nuclear Material Handling,</li>
				<li class="general_paragraph">Nuclear Licensing, Strategic Regulatory Counsel,</li>
				<li class="general_paragraph">Significance Determination Process,</li>
				<li class="general_paragraph">Preparation and Support for Enforcement & Regulatory Conferences</li>
				<li class="general_paragraph">and Preparation and Support for ALL NRC Inspections. Including:  95001, 95002 and 95003.</li>
			</ul>
			<p class="general_paragraph">Conger-Elsea also provides the Training Solutions for each, enabling your team to manage these significant performance improvement initiatives.  All aspects of our business are characterized by a legacy of ethics and unmatched expertise from our founders, Dorian Conger and Ken Elsea; whose fingerprints are indelibly fixed to all the firm’s content and services.  When you’re searching for the real solution that will mitigate risk, enhance the safety culture and improve performance … </p>
			<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
		</div>
		<hr class="custom_hr after_opp_adv_section">
		<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<div class="bottom_gradient"></div>
	</body>
</html><?php }} ?>
