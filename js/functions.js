jQuery(document).ready(function(){

	// Initiate BxSlider
	jQuery('.bxslider').bxSlider();

	// Initiate SlickNav	  
	jQuery('#menu').slicknav();
	

   // Define Toggle Function on clicking
   $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };

    

    //Small Resolution Slider at the Bottom of the Homepage
	jQuery('.next_small_res').click(
		next
	);
	jQuery('.prev_small_res').click(
		prev
	);
	function next() {
		var left_value = jQuery('.opportunities_row .width_full_small_res+.width_full_small_res').css('left');
		if( left_value != 0 ) {
			jQuery('.opportunities_row .width_full_small_res').css('left', '-190px');
			jQuery('.opportunities_row .width_full_small_res+.width_full_small_res').css('left', '0');
		}
		if( left_value == '0px' ) {
			jQuery('.opportunities_row .width_full_small_res').css('left', '0');
			jQuery('.opportunities_row .width_full_small_res+.width_full_small_res').css('left', '190px');	
		}
	}
	function prev() {
		var left_value = jQuery('.opportunities_row .width_full_small_res+.width_full_small_res').css('left');
		if( left_value != 0 ) {
			jQuery('.opportunities_row .width_full_small_res').css('left', '-190px');
			jQuery('.opportunities_row .width_full_small_res+.width_full_small_res').css('left', '0');
		}
		if( left_value == '0px' ) {
			jQuery('.opportunities_row .width_full_small_res').css('left', '0');
			jQuery('.opportunities_row .width_full_small_res+.width_full_small_res').css('left', '190px');	
		}
	}




	// Mobile Menu SELECT (Courses) clicking
	jQuery('.select_courses.small_res').clickToggle(show_select_content, hide_select_content);
	function show_select_content() {
		jQuery(this).parent().next().hide();
		jQuery(this).css({
			'background-color': '#116eb7',
			'background-position': '4% -34px'
		});
		jQuery(this).next().show();
	}
	function hide_select_content() {
		jQuery(this).parent().next().show();
		jQuery(this).css({
			'background-color': '#092c74',
			'background-position': '220px 0'
		});
		jQuery(this).next().hide();
	}


	// Mobile Menu Item Clicking	
	jQuery('.slicknav_nav .menu_items > li > a').clickToggle(show_content, hide_content);
	function show_content() {
		jQuery(this).css('background-position', '15px -34px');
		jQuery(this).parent().siblings().hide();
		jQuery(this).parent().parent().prev().find('.select_courses.small_res').hide();
	}
	function hide_content() {
		jQuery(this).css('background-position', '220px 0');
		jQuery(this).parent().siblings().show();
		jQuery(this).parent().parent().prev().find('.select_courses.small_res').show();
	} 


	// Scale dimensions on load
	jQuery(window).on('load', function() {
		// About processes page
		var process_pic_width = jQuery('.process_sprites .process_strategy_sprite').width();
		jQuery('.process_sprites .process_strategy_sprite').css('height', process_pic_width+20+'px');
		// About Strategy page
		var strategy_pic_width = jQuery('.pos_strategy').width();
		jQuery('.pos_strategy').css('height', strategy_pic_width+'px');		
			// Markets Main page
		var markets_main_container_width = jQuery('.col-md-4 .markets_main_container .markets_sprite').width();
		jQuery('.col-md-4 .markets_main_container .markets_sprite').css('height', markets_main_container_width+'px');
			// Markets Top image
		var markets_main_container_width = jQuery('.markets_top_pos').width();
		jQuery('.markets_top_pos').css('height', markets_main_container_width/3.1+'px');
			// Markets Emblemmas
		var markets_main_container_width = jQuery('.energy_emblemma, .gov_reg_emblemma, .priv_comm_emblemma').width();
		jQuery('.energy_emblemma, .gov_reg_emblemma, .priv_comm_emblemma').css('height', markets_main_container_width+'px');
			// Relationships Top image
		var markets_main_container_width = jQuery('.relationships_top_pos').width();
		jQuery('.relationships_top_pos').css('height', markets_main_container_width/3.1+'px');
			// Relationships Main Links
		var markets_main_container_width = jQuery('.row.relationships_main .relationships_main_container .relationships_sprite').width();
		jQuery('.row.relationships_main .relationships_main_container .relationships_sprite').css('height', markets_main_container_width+'px');
			// Relationships Federal-State-International Sprites
		var markets_main_container_width = jQuery('.fed_state_int_row .relationships_sprite').width();
		jQuery('.fed_state_int_row .relationships_sprite').css('height', markets_main_container_width+'px');

	});


	jQuery(window).on('resize', function() {

		// Go back to default settings on resize
			//Home page Opportunities
		if( jQuery(window).width() > 580 ) {
			jQuery('.opportunities_row .width_full_small_res').css('left', '0');
		}
		if( jQuery(window).width() <= 580 ) {
			jQuery('.opportunities_row .width_full_small_res+.width_full_small_res').css('left', '190px');
		}

		// Scale dimensions on Resize
			// About processes page
		var process_pic_width = jQuery('.process_sprites .process_strategy_sprite').width();
		jQuery('.process_sprites .process_strategy_sprite').css('height', process_pic_width+20+'px');
			// About Strategy page
		var strategy_pic_width = jQuery('.pos_strategy').width();
		jQuery('.pos_strategy').css('height', strategy_pic_width+'px');
			// Markets Main page
		var markets_main_container_width = jQuery('.col-md-4 .markets_main_container .markets_sprite').width();
		jQuery('.col-md-4 .markets_main_container .markets_sprite').css('height', markets_main_container_width+'px');
			// Markets Top image
		var markets_main_container_width = jQuery('.markets_top_pos').width();
		jQuery('.markets_top_pos').css('height', markets_main_container_width/3.1+'px');
			// Markets Emblemmas
		var markets_main_container_width = jQuery('.energy_emblemma, .gov_reg_emblemma, .priv_comm_emblemma').width();
		jQuery('.energy_emblemma, .gov_reg_emblemma, .priv_comm_emblemma').css('height', markets_main_container_width+'px');
			// Relationships Top image
		var markets_main_container_width = jQuery('.relationships_top_pos').width();
		jQuery('.relationships_top_pos').css('height', markets_main_container_width/3.1+'px');
			// Relationships Main Links
		var markets_main_container_width = jQuery('.row.relationships_main .relationships_main_container .relationships_sprite').width();
		jQuery('.row.relationships_main .relationships_main_container .relationships_sprite').css('height', markets_main_container_width+'px');
			// Relationships Federal-State-International Sprites
		var markets_main_container_width = jQuery('.fed_state_int_row .relationships_sprite').width();
		jQuery('.fed_state_int_row .relationships_sprite').css('height', markets_main_container_width+'px');

	});


	// TABS Functionality
	jQuery(document).ready(function() {
    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');
 
        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });
});


});
















// if(jQuery(window).width()<1150) {
	// jQuery(window).on('load', heights);
	// jQuery(window).on('resize', heights);
// }


// function heights() {
// 	if( jQuery('.j_title').length ) {
	
// 		var j_title = jQuery('.j_title');
// 		var height = 0;

// 		j_title.each(function(i) {
// 			var single_title = jQuery(j_title[i]);
// 			if(height<single_title.height() ) {
// 				height = single_title.height();
// 			}
// 		});

// 		jQuery('.j_title').css('height', height);
// 	}

// 	if( jQuery('.j_row').length ) {
	
// 		var j_row = jQuery('.j_row');
// 		var height = 0;

// 		j_row.each(function(i) {
// 			var single_row = jQuery(j_row[i]);
// 			if(height<single_row.height() ) {
// 				height = single_row.height();
// 			}
// 		});

// 		jQuery('.j_row').css('height', height);
// 	}

// }
