<?php

// Include config FILE
include('inc/config.php');


// Get and set $action var
$action = $_GET['action'];
$view = $_GET['view'];
switch($action) {
    case 'about':
    case 'services':
    case 'markets':
    case 'relationships':
    case 'resources':
        break;
    default: 
        $action = 'main'; 
}

// Build url
$url = '?page={$page_number}';

// Assign Homepage link
$smarty->assign('home_link', 'http://conger-elsea-simple.linuxserv.space/');

// Assign Logo path
$smarty->assign('logos_path', 'images/logos/');
// $smarty->assign('images_path', 'images/');



// Selecting Templates By $action
if($action == 'main') { 
    $smarty->display('head.tpl');
    $smarty->display('body_main.tpl'); 
} 


// About Page Selection
if($action == 'about') {

    switch($view) {
        case 'team':
        case 'strategy':
        case 'process':
        case 'ethics':
            break;
        default: 
            $view = 'main'; 
    }

    $smarty->assign('about_view', $view);

    $smarty->display('head.tpl');
    $smarty->display('about/body_about.tpl'); 

}


// Services Page Selection
if($action == 'services') {

    switch($view) {
        case 'training':
        case 'consulting':
            break;
        default: 
            $view = 'main'; 
    }

    $smarty->assign('services_view', $view);

    $smarty->display('head.tpl');
    $smarty->display('services/body_services.tpl'); 

}


// Markets Page Selection
if($action == 'markets') {

    switch($view) {
        case 'energy':
        case 'private-commercial':
        case 'government-regulatory':
            break;
        default: 
            $view = 'main'; 
    }

    $smarty->assign('markets_view', $view);

    $smarty->display('head.tpl');
    $smarty->display('markets/body_markets.tpl'); 

}



// Relationships Page Selection
if($action == 'relationships') {

    switch($view) {
        case 'federal-state-international':
        case 'testimonials':
            break;
        default: 
            $view = 'main'; 
    }

    $smarty->assign('relationships_view', $view);

    $smarty->display('head.tpl');
    $smarty->display('relationships/body_relationships.tpl'); 

}



// Resources Page Selection
if($action == 'resources') {

    $smarty->display('head.tpl');
    $smarty->display('resources/body_resources.tpl'); 

}