<body class="relationships_page">
		<div class="gradient_top"></div>
		{include file='header.tpl'}
		{include file='relationships/top_side.tpl'}
		<div class="white_background_container">
			{if $relationships_view == "federal-state-international"}
				{include file='relationships/relationships_federal-state-international_content.tpl'}
			{/if}
			{if $relationships_view == "testimonials"}
				{include file='relationships/relationships_testimonials_content.tpl'}
			{/if}
			{if $relationships_view == "main"}
				{include file='relationships/relationships_main_content.tpl'}
			{/if}
		</div>
		<hr class="custom_hr after_opp_adv_section">
		{include file='footer.tpl'}
		<div class="bottom_gradient"></div>
	</body>
</html>
