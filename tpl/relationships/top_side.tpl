<div class="top_image">
	<div class="relationships_sprite relationships_top_pos"></div>
</div>
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-6 clear-col_980">
		<div class="training_container sprite training_pos">
			<div class="training_button">
				<a href="#">TRAINING</a>
			</div>
		</div>	
		<div class="training_button_small_res">
			<a href="#">TRAINING</a>
		</div>			
	</div>			
	<div class="col-md-6 col-sm-6 col-xs-6 clear-col_980">
		<div class="consulting_container sprite consulting_pos">
			<div class="consulting_button">
				<a href="#">CONSULTING</a>
			</div>
		</div>
		<div class="consulting_button_small_res">
			<a href="#">CONSULTING</a>
		</div>	
	</div>				
</div>