<div class="the_page_title">RELATIONSHIPS</div>
<p class="general_paragraph">In today's competitive business environment, enterprise companies need to remain focused on an area of expertise. Conger-Elsea recognizes the need to foster and develop relationships with emerging organizations as a strategic alliance or a partnership that offer complementary strengths. Conger-Elsea relationships allow us to deliver broad solutions that will deliver integrated, robust products and services to address your business challenges, drive your revenue and exploit new market opportunities. Want to learn more about these premier listings of “Relationships”, and how Conger-Elsea can work for you? Contact Us: 800-875-8709 info@conger-elsea.com<br /><br />Conger-Elsea has assisted and created value for more than 200 clients and 12,000 professionals who have successfully completed and continue to benefit from our programs and solutions. C-E relationships are separated into these easy-to-refrence service areas.</p>
<div class="row relationships_main">
	<div class="col-md-2 col-sm-2">
	</div>
	<div class="col-md-4 col-sm-4">
		<div class="relationships_main_container">
			<div class="relationships_sprite federal_state_international_pos"></div>
			<div class="relationships_main_overlay">
				<div class="relationships_overlay_text">GVERNMENT FEDERAL, STATE, INTERNATIONAL</div>
				<a href="?action=relationships&view=federal-state-international"></a>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		<div class="relationships_main_container">
			<div class="relationships_sprite testimonials_pos"></div>
			<div class="relationships_main_overlay">
				<div class="relationships_overlay_text">PRIVATE/ COMERCIAL</div>
				<a href="?action=relationships&view=testimonials"></a>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-2">
	</div>
</div>
<hr class="custom_hr">
<p class="general_paragraph">Conger-Elsea is recognized as the premier provider of go-to TRAINING and CONSULTING solutions and services for: RCA - Root Cause Analysis/incident Investigation, Corrective Action, Safety/Safety Culture, Human Performance Improvement, Regulatory Compliance and Education. To name a few C-E is always in search of qualified partnerships who together will add solutions and strength to your business.</p>
<div class="hellier_container">
	<div class="hellier_logo">
		<img src="{$logos_path}hellier_logo.png" alt="hellier_logo"/>
	</div>
	<div class="hellier_description">
		<div class="maroon_small_top">NONDESTRUCTIVE TESTING</div>
		<div class="big_black_bottom">NDT TRAINING AND CONSULTING</div>
	</div>
	<div class="hellier_button"><a href="#">VISIT HELLIER</a></div>
</div>
<hr class="custom_hr">
<div class="relationships_main_contact">
	<div class="relationships_main_contact_question">Want to learn more about these premier "relationships", and how Conger-Elsea can work for you?</div>
	<div class="relationships_main_contact_links">Contact Us: <a href='tel:8008758709'>800-875-8709</a> or <a href="info@conger-elsea.com">email</a></div>
</div>
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>