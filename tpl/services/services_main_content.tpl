<div class="row">
	<div class="col-md-8 col-sm-8 clear_both_980">
		<div class="the_page_title">SERVICES</div>
		<p class="general_paragraph">Conger-Elsea  … Provides Training and Consulting Services for Identifying, Analyzing, Evaluating and Advising on Matters of Risk.  Industry leaders from around the world embrace and practice these proven methods to evaluate root causes, assess safety culture, and develop corrective actions for accidents, incidents and regulatory compliance.</p>
	</div>
	<div class="col-md-4 col-sm-4 clear_both_980">
		<div class="services_main_right">
			<img src="../images/services/services_main_right.jpg" alt="services_main_right.jpg"/>
		</div>
	</div>
</div>
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
