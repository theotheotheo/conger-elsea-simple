<body class="services_page">
		<div class="gradient_top"></div>
		{include file="header.tpl"}
    {include file="services/top_side.tpl"}
		<div class="white_background_container">
      {if $services_view == 'training'}
        {include file="services/services_training_content.tpl"}
      {/if}
      {if $services_view == 'consulting'}
        {include file="services/services_consulting_content.tpl"}
      {/if}
      {if $services_view == 'main'}
        {include file="services/services_main_content.tpl"}
      {/if}
		</div>
		<hr class="custom_hr after_opp_adv_section">
		{include file='footer.tpl'}
		<div class="bottom_gradient"></div>
	</body>
</html>
