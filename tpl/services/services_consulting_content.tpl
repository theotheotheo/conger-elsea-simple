<div class="the_page_title">SERVICES - CONSULTING</div>
<p class="general_paragraph">A refined process of experience, insight, skills and the resources required to address YOUR specific issues.  To name a few, Conger-Elsea’s consulting activities include:</p>
<div class="row services_training">
	<div class="col-md-6 col-sm-6">
		<ul class="general_ul">
			<li class="general_paragraph">Root Cause Analysis / Incident Investigation</li>
			<li class="general_paragraph">Corrective Action / Development / Improvemet</li>
			<li class="general_paragraph">Safety / Safety Culture Assessment</li>
			<li class="general_paragraph">Human Performence Improvement</li>
			<li class="general_paragraph">Audits and Evaluations</li>
			<li class="general_paragraph">Nuclear Licensing</li>
		</ul>
	</div>
	<div class="col-md-6 col-sm-6">
		<ul class="general_ul">
			<li class="general_paragraph">Strategic Regulatory Counsel</li>
			<li class="general_paragraph">Preparation and Support for ALL NRC Inspections Including 95001, 95002 and 95003</li>
			<li class="general_paragraph">Segnificance Determination Process</li>
			<li class="general_paragraph">Preparation and Support for Enforcement and Regulatory Conferences</li>
			<li class="general_paragraph">Regulatory Compliance</li>
		</ul>
	</div>
</div>
<p class="bottom_normal_blue_paragraph">Conger Elsea … Managing Risk.  Enhancing Culture.  Improving Performance.</p>
