<div class="row">	
	<div class="col-md-7 col-sm-7">
		<div class="the_page_title">MARKETS - PRIVATE/COMMERCIAL</div>
		<p class="general_paragraph">Comprehensive support services and tools for:  Pharmaceutical/Healthcare Facilities using Radiation-Based Products/Services and other Private/Commercial Industries.  Such as:  Agriculture, Manufacturing and Transportation to name a few.  Conger-Elsea provides relevant support services and tools for all. </p>
		<ul class="general_ul">
			<li class="general_paragraph">Pharmaceutical</li>
			<li class="general_paragraph">Healthcare</li>
			<li class="general_paragraph">Other Industries</li>
		</ul>
	</div>
	<div class="col-md-5 col-sm-5">
		<div class="markets_sprite priv_comm_emblemma"></div>
	</div>
</div>
