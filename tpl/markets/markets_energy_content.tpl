<div class="row">	
	<div class="col-md-7 col-sm-7">
		<div class="the_page_title">MARKETS - ENERGY</div>
		<p class="general_paragraph">Conger-Elsea provides comprehensive support services and tools for:  Public and Private Power Generation and Distribution, Oil and Natural Gas, Pipeline Management Companies and support for Nuclear Materials Industries.</p>
		<ul class="general_ul">
			<li class="general_paragraph">Power Generation & Distribution</li>
			<li class="general_paragraph">Oil & Natural Gas</li>
			<li class="general_paragraph">Pipeline</li>
			<li class="general_paragraph">Nuclear Materials</li>
		</ul>
	</div>
	<div class="col-md-5 col-sm-5">
		<div class="markets_sprite energy_emblemma"></div>
	</div>
</div>
