<div class="row">	
	<div class="col-md-7 col-sm-7">
		<div class="the_page_title">MARKETS - GOVERNMENT/REGULATORY</div>
		<p class="general_paragraph">C-E also provides end-to-end support services and tools for:  Government Oversight/Support and Regulatory Agencies.</p>
		<ul class="general_ul">
			<li class="general_paragraph">Federal</li>
			<li class="general_paragraph">State</li>
			<li class="general_paragraph">International</li>
		</ul>
	</div>
	<div class="col-md-5 col-sm-5">
		<div class="markets_sprite gov_reg_emblemma"></div>
	</div>
</div>
