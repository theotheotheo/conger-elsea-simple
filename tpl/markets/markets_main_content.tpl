<div class="the_page_title">MARKETS</div>
<p class="general_paragraph">Conger-Elsea separates market support into three relevant areas. Each strategically enabled to direct Domestic and International services.</p>
<div class="row markets_main">
	<div class="col-md-4 col-sm-4">
		<div class="markets_main_container">
			<div class="markets_sprite energy_pos"></div>
			<div class="markets_main_overlay">
				<div class="markets_overlay_text">ENERGY MARKETS</div>
				<a href="?action=markets&view=energy"></a>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		<div class="markets_main_container">
			<div class="markets_sprite gov_reg_pos"></div>
			<div class="markets_main_overlay">
				<div class="markets_overlay_text">GOVERNMENT REGULATORY MARKETS</div>
				<a href="?action=markets&view=government-regulatory"></a>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-4">
		<div class="markets_main_container">
			<div class="markets_sprite priv_comm_pos"></div>
			<div class="markets_main_overlay">
				<div class="markets_overlay_text">PRIVATE COMMERCIAL MARKETS</div>
				<a href="?action=markets&view=private-commercial"></a>
			</div>
		</div>
	</div>
</div>
<p class="general_paragraph">As part of each market we effectively deploy specially designed services and products that will provide real solutions, for you, our client.  We share our resources to build long-term relationships. </p>
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
