<body class="markets_page">
		<div class="gradient_top"></div>
		{include file="header.tpl"}
		{include file='markets/top_side.tpl'}
		<div class="white_background_container">
			{if $markets_view == 'energy'}
				{include file="markets/markets_energy_content.tpl"}
			{/if}
			{if $markets_view == 'private-commercial'}
				{include file="markets/markets_private-commercial_content.tpl"}
			{/if}
			{if $markets_view == 'government-regulatory'}
				{include file="markets/markets_government-regulatory_content.tpl"}
			{/if}
			{if $markets_view == 'main'}
				{include file="markets/markets_main_content.tpl"}
			{/if}
		</div>
		<hr class="custom_hr after_opp_adv_section">
		{include file='footer.tpl'}
		<div class="bottom_gradient"></div>
	</body>
</html>
