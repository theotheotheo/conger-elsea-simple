	<body class="resources_page">
		<div class="gradient_top"></div>
		{include file='header.tpl'}
		{include file='resources/top_side.tpl'}
		<div class="white_background_container">
			{include file='resources/resources_main_content.tpl'}
		</div>
		<hr class="custom_hr after_opp_adv_section">
		{include file='footer.tpl'}
		<div class="bottom_gradient"></div>
	</body>
</html>
