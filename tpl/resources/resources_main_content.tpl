<div class="the_page_title">RESOURCES</div>
<p class="general_paragraph">Conger-Elsea specializes in the assessment of organizational strengths and weaknesses, the processes that influence team performance, and the development of intervention strategies to address areas identified for improvement.  In part, the assessment includes a measurement of performance to ensure effectiveness, particularly in organizations that engage in complex, high-risk operations with the potential to impact employee safety and public health.  Conger-Elsea has extensive domestic and international experience in qualitatively and quantitatively measuring the organizational processes AND behaviors which have the greatest impact on safety and performance. </p>
<hr class="custom_hr">
<div class="tabs">
    <ul class="tab-links">
        <li class="active"><a href="#tab1">TRAINING</a></li>
        <li><a href="#tab2">CONSULTING</a></li>
    </ul>
 
    <div class="tab-content">
        <div id="tab1" class="tab active"> 
        	<div class="row"> 
	        	<div class="col-md-8 col-sm-8">
					<p class="general_paragraph">The Conger-Elsea training curricula has been presented to more than 18,000 industry professionals who have been certified in the use of this invaluable tool for:</p>          
					<ul class="general_ul">
						<li class="general_paragraph">Root Cause Analysis / Incident Investigation</li>
						<li class="general_paragraph">Management Oversight and Risk Tree (MORT)</li>
					</ul>
					<p class="general_paragraph">Conger-Elsea also provides specialized training in the areas of:</p>      
					<ul class="general_ul">
						<li class="general_paragraph">Safety/Safety Culture Training</li>
						<li class="general_paragraph">Human Performance Improvement</li>
						<li class="general_paragraph">Regulatory Communication, Compliance and Education</li>
					</ul>
					<p class="general_paragraph">C-E has established strategic relationships with a host of U.S. and international clients where the truth, accuracy and fact-based analyses are critical and essential.  The training workshop experience is carefully crafted to match your needs …Not a one-size-fits-all approach for YOUR training solutions.  The Conger-Elsea team tailors each engagement to meet and exceed the clients’ expectations in managing risks, enhancing culture and improving performance.</p>
				</div>
				<div class="col-md-4 col-sm-4">
				<div class="sprite resources_pic_training">
						<div class="overlay_colour"></div>
						<div class="overlay_text">TRAINING</div>
					</div>
				</div>
			</div>
        </div>
 
        <div id="tab2" class="tab">
        	<div class="row"> 
	        	<div class="col-md-8 col-sm-8">
					<p class="general_paragraph">Identifying, analyzing, evaluating and advising on matters of risk takes many forms.  It’s a refined process of experience, insight, skills and the resources required to address YOUR specific issues. To name a few, Conger-Elsea consulting activities have included:  </p>
					<ul class="general_ul">
						<li class="general_paragraph">Root Cause Analysis/Incident Investigation</li>
						<li class="general_paragraph">Corrective Action/Development/Improvement</li>
						<li class="general_paragraph">Safety/Safety Culture Assessment</li>
						<li class="general_paragraph">Audits and Evaluations</li>
						<li class="general_paragraph">Nuclear Material Handling</li>
						<li class="general_paragraph">Nondestructive Testing - NDT</li>
						<li class="general_paragraph">Nuclear Licensing</li>
						<li class="general_paragraph">Strategic Regulatory Counsel</li>
						<li class="general_paragraph">Significance Determination Process</li>
						<li class="general_paragraph">Preparation and Support for Enforcement & Regulatory Conferences</li>
						<li class="general_paragraph">Preparation and Support for ALL NRC Inspections<br /> Including: 95001, 95002 and 95003 </li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="sprite resources_pic_consulting">
						<div class="overlay_colour"></div>
						<div class="overlay_text">CONSULTING</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
<hr class="custom_hr">
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
