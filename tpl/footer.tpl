<footer>	
	<div class="solution_partner">
		<div class="white_main_logo">
			<img src="{$logos_path}white_main_logo.png" alt="white_main_logo"/>
		</div>
		<div class="newsleter_text">
			<div class="newsletter_big">Join our newsletter.<span class="newsletter_small">Get exclusive content, tips, and special offers.</span></div>			
		</div>
		<div class="join_newsletter_button"><a href="#">JOIN NOW</a></div>
	</div>
	<div class="contact_data">
		<ul class="contact_data_ul">
			<li class="contact_data_li">
				<div class="li_title">Find:</div>
				<div class="li_main_content">3440 Blue Springs Rd., Ste. 102<br />Kennesaw, GA, 30144</div>
			</li>
			<li class="contact_data_li">
				<div class="li_title">Call:</div>
				<div class="li_main_content"><a href="tel:8008758709">Toll free: 800-875-8709</a><br /><a href="Voice:7709261131">Voice: 770-926-1131</a></div>
			</li>
			<li class="contact_data_li">
				<div class="li_title">Email:</div>
				<div class="li_main_content"><a href="mailto:info@conger-elsea.com">info@conger-elsea.com</a></div>
			</li>
			<li class="contact_data_li">
				<div class="li_title">Follow Us:</div>
				<div class="li_main_content"><a href="#"><img class="linkedin_logo" src="{$logos_path}linkedin_logo.png"/></a></div>
			</li>
			<li class="contact_data_li">
				<div class="li_title">View Our Home Website:</div>
				<div class="li_main_content">
					<img class="white_advantae_logo" src="{$logos_path}white_advantage_logo.png" alt="white_advantae_logo"/>
				</div>
			</li>
		</ul>
	</div>	
	<div class="footer_last_row">
		<div class="main_menu footer">
			<a href="#" class="menu_item">ABOUT US</a>
			<a href="#" class="menu_item">SERVICES</a>
			<a href="#" class="menu_item">MARKETS</a>
			<a href="#" class="menu_item">RELATIONSHIPS</a>
			<a href="#" class="menu_item">RESOURCES</a>
			<a href="#" class="menu_item">CAREERS</a>
			<a href="#" class="menu_item">CONTACT US</a>
		</div>
		<div class="credits">Website Developed and Hosted by Digital Lagoon</div>
	</div>
</footer>