<!DOCTYPE html>
<html>
<head>
	<title>Conger-Elsea</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,700italic,400,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">

	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1, user-scalable=yes, minimal-ui" />

	<script type="text/javascript" src="/js/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>

	{*SlickNav*}
	<link rel="stylesheet" href="SlickNav/scss/slicknav.scss" />
	<script src="SlickNav/jquery.slicknav.js"></script>

	<!-- bxSlider Javascript file -->
	<script src="/assets/bxslider/jquery.bxslider.min.js"></script>
	<!-- bxSlider CSS file -->
	<link href="/assets/bxslider/jquery.bxslider.css" rel="stylesheet" />

	<link href='font-awesome-4.5.0/css/font-awesome.css' rel='stylesheet' type='text/css'>
	<link href='css/style.css' rel='stylesheet' type='text/css'>




</head>