<div class="header">
	<div class="logo">
	<img class="" src="{$logos_path}main_logo.png">
		<a href="{$home_link}"></a>
	</div>
	<ul id="menu">
		<!-- <div class="menu_items"> -->
		<div class="top_features">
			<select name="COURSES" class="select_courses">
			  <option selected value="">Courses</option>
			  <option value="course-1">Course 1</option>
			  <option value="course-2">Course 2</option>
			  <option value="course-3">Course 3</option>
			  <option value="course-4">Course 4</option>
			</select>
			<input type="text" class="search_box_input" placeholder="Search">
			<input type="button" class="login_button" name="login_button" value="LOGIN">
			<div class="select_courses small_res">COURSES</div>
			<div class="options_container">
			  <div class="courses_option"><a href="#">Course 1</a></div>
			  <div class="courses_option"><a href="#">Course 2</a></div>
			  <div class="courses_option"><a href="#">Course 3</a></div>
			  <div class="courses_option"><a href="#">Course 4</a></div>
			</div>
		</div>
		<span class="menu_items">
			<li>
				<a href="?action=about" class="menu_item">ABOUT US</a>
		        <ul>
		            <li><a href="?action=about&view=team">Team</a></li>
		            <li><a href="?action=about&view=strategy">Strategy</a></li>
		            <li><a href="?action=about&view=process">Process</a></li>
		            <li><a href="?action=about&view=ethics">Ethics</a></li>
		        </ul>
		    </li>
		    <li>
		    	<a href="?action=services" class="menu_item">SERVICES</a>
		        <ul>
		            <li><a href="?action=services&view=training">Training</a></li>
		            <li><a href="?action=services&view=consulting">Consulting</a></li>
		        </ul>
		    </li>
		    <li>
		    	<a href="?action=markets" class="menu_item">MARKETS</a>
		    	 <ul>
		            <li><a href="?action=markets&view=energy">Energy Markets</a></li>
		            <li><a href="?action=markets&view=private-commercial">Private / Commercial Markets</a></li>
		            <li><a href="?action=markets&view=government-regulatory">Government / Regulatory Markets</a></li>
		        </ul>
		    </li>
		    <li>
		    	<a href="?action=relationships" class="menu_item">RELATIONSHIPS</a>
		    	 <ul>
		            <li><a href="?action=relationships&view=federal-state-international">Federal / State / International</a></li>
		            <li><a href="?action=relationships&view=testimonials">Testimonials</a></li>
		        </ul>
		    </li>
		    <li>
		    	<a href="?action=resources" class="menu_item">RESOURCES</a>
		    </li>
		    <li>
		    	<a href="#" class="menu_item">CAREERS</a>
		    </li>
		    <li>
		    	<a href="#" class="menu_item">CONTACT US</a>
		    </li>
		</span>
	</ul>

</div>