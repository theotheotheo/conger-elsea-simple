<div class="the_page_title">ABOUT US</div>
<p class="general_paragraph">Conger-Elsea represents a vast collection of expertise from private businesses and government agencies with an impeccable history of success.  The firm includes over sixty experienced professionals with an in-depth knowledge of: </p>
<ul class="general_ul">
	<li class="general_paragraph">Root Cause Analysis/Incident Investigation,</li>
	<li class="general_paragraph">Corrective Action/Development/Improvement,</li>
	<li class="general_paragraph">Safety/Safety Culture Assessments,</li>
	<li class="general_paragraph">Audits and Evaluations,</li>
	<li class="general_paragraph">Nuclear Material Handling,</li>
	<li class="general_paragraph">Nuclear Licensing, Strategic Regulatory Counsel,</li>
	<li class="general_paragraph">Significance Determination Process,</li>
	<li class="general_paragraph">Preparation and Support for Enforcement & Regulatory Conferences</li>
	<li class="general_paragraph">and Preparation and Support for ALL NRC Inspections. Including:  95001, 95002 and 95003.</li>
</ul>
<p class="general_paragraph">Conger-Elsea also provides the Training Solutions for each, enabling your team to manage these significant performance improvement initiatives.  All aspects of our business are characterized by a legacy of ethics and unmatched expertise from our founders, Dorian Conger and Ken Elsea; whose fingerprints are indelibly fixed to all the firm’s content and services.  When you’re searching for the real solution that will mitigate risk, enhance the safety culture and improve performance … </p>
<p class="bottom_normal_blue_paragraph">Conger-Elsea … Your Solutions Partner.</p>
