<body class="about_page">
		<div class="gradient_top"></div>
		{include file='header.tpl'}
		{include file='about/top_side.tpl'}
		<div class="white_background_container">
			{if $about_view == "team"}
				{include file='about/about_team_content.tpl'}
			{/if}
			{if $about_view == "strategy"}
				{include file='about/about_strategy_content.tpl'}
			{/if}
			{if $about_view == "process"}
				{include file='about/about_process_content.tpl'}
			{/if}
			{if $about_view == "ethics"}
				{include file='about/about_ethics_content.tpl'}
			{/if}
			{if $about_view == "main"}
				{include file='about/about_main_content.tpl'}
			{/if}
		</div>
		<hr class="custom_hr after_opp_adv_section">
		{include file='footer.tpl'}
		<div class="bottom_gradient"></div>
	</body>
</html>
