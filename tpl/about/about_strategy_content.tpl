<div class="row">
	<div class="col-md-8 col-sm-8 clear_both_980">
		<div class="the_page_title">ABOUT US - STRATEGY</div>
		<p class="general_paragraph">Conger-Elsea develops “leaders” who guide, support, encourage and enable team members to reach their fullest potential.  We’re not focused purely on oversight.  We strategize, refocus and consult with YOUR team.  Elevating their skill sets from rote compliance to full comprehension of the regulatory trends, industry issues and the approved methods for reducing risk while building a culture of only the highest performance and productivity.</p>
	</div>
	<div class="col-md-4 col-sm-4 clear_both_980">
		<div class="process_strategy_sprite pos_strategy"></div>
	</div>
</div>
<div class="blank_40"></div>
