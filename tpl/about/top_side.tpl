<div class="top_image">
	{if $about_view =='main' }
		<img src="images/about/about_main_top.jpg" alt="about_pic"/>
	{else}
		<img src="images/about/about_secondary_top.jpg" alt="about_pic"/>
	{/if}
</div>
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-6 clear-col_980">
		<div class="training_container sprite training_pos">
			<div class="training_button">
				<a href="#">TRAINING</a>
			</div>
		</div>	
		<div class="training_button_small_res">
			<a href="#">TRAINING</a>
		</div>			
	</div>			
	<div class="col-md-6 col-sm-6 col-xs-6 clear-col_980">
		<div class="consulting_container sprite consulting_pos">
			<div class="consulting_button">
				<a href="#">CONSULTING</a>
			</div>
		</div>
		<div class="consulting_button_small_res">
			<a href="#">CONSULTING</a>
		</div>	
	</div>				
</div>
