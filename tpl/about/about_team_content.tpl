<div class="the_page_title">ABOUT US - TEAM</div>
<div class="row team_member_row" id="dorian_conger_row">
	<div class="col-md-3 col-sm-3 clear_both_1140">
		<div class="team_outer_border">
			<div class="team_sprite dorian_conger"></div>
		</div>
	</div>
	<div class="col-md-9 col-sm-9 clear_both_1140">
		<div class="team_member_name">Dorian Conger</div>
		<div class="team_member_role">Founder and Advisory Board Chairman</div>
		<div class="see_at_all_res">
			<p class="general_paragraph">Dorian is an internationally recognized expert in incident investigation, root cause analysis, safety behavior and human <span class="read_more_dots">...</span></p>
		</div>
		<a href="#dorian_conger_row" class="read_more_res">Read more</a>
		<div class="see_at_read_more"> 
			<p class="general_paragraph">performance. During his more than thirty years in business, he has participated in projects and training for clients in: Antarctica, Australia, South America, Asia, Europe, and North America.  He has led major investigations at commercial and government facilities including:  Aerospace, Chemical, Energy, Manufacturing, Petroleum and Utilities.  Dorian has conducted training programs for more than 200 client organizations and he developed the Model Root Cause Analysis Methodologies, and Corrective Action Programs that have become the global standard for the energy industries.<br /><br />As a developer/instructor, he also led the first Institute of Nuclear Power Operations (INPO) training program on incident investigation and root cause analysis and participated in the initial development of the Human Performance Enhancement System (HPES).  He was instrumental in the development of the Mishap Analysis and Prevention System (MAPS) software which contains a series of safety analytics for conducting safety inspections and root cause analysis.  In 1992, with Ken Elsea, Dorian purchased the division of EG&G, an energy consulting firm for which they worked and then launched Conger & Elsea, Incorporated.</p>
			<a href="#dorian_conger_row_closed" class="read_less_res">Read less</a>
		</div>	
	</div>
</div>
<div class="row team_member_row margin_top_50" id="eugene_cobey_row">
	<div class="col-md-3 col-sm-3 clear_both_1140">
		<div class="team_outer_border">
			<div class="team_sprite eugene_cobey"></div>
		</div>
	</div>
	<div class="col-md-9 col-sm-9 clear_both_1140">
		<div class="team_member_name">Eugene Cobey</div>
		<div class="team_member_role">President and Chief Executive Officer </div>
		<div class="see_at_all_res">
			<p class="general_paragraph">Gene is a seasoned leadership executive with over 25 years of experience and success in nuclear safety regulation, nuclear <span class="read_more_dots">...</span></p>
		</div>
		<a href="#eugene_cobey_row" class="read_more_res">Read more</a>
		<div class="see_at_read_more"> 
			<p class="general_paragraph">licensing and conduct of operations.  He brings expertise and a myriad of relationships in the following sectors: nuclear regulatory and safety inspection, performance assessment of fuel cycle facilities, nuclear material licensees and reactor operations.<br /><br />Since serving in the U.S. Navy as a nuclear trained submarine warfare officer, Gene held a variety of leadership and executive roles for the U.S. Nuclear Regulatory Commission.  Since then, he held positions of leadership and management responsibility as part of the Tennessee Valley Authority; where he served as the Browns Ferry Director of Licensing during the 95003 recovery effort, the General Manager of Regulatory Operations, and the Senior Manager of New Plant Licensing and Regulatory Improvement.<br /><br />He serves as Conger-Elsea’s strategic visionary with a clear sense of purpose and urgency.   With an extensive background in the nuclear industry, Gene has a proven ability to successfully analyze an organization's critical business requirements, identify deficiencies and potential opportunities, and develop innovative and cost-effective solutions for enhancing competitiveness, increasing profitability, and improving customer service offerings. </p>
			<a href="#eugene_cobey_row_closed" class="read_less_res">Read less</a>
		</div>
	</div>
</div>
<div class="row team_member_row margin_top_50" id="roy_caniano_row">
	<div class="col-md-3 col-sm-3 clear_both_1140">
		<div class="team_outer_border">
			<div class="team_sprite roy_caniano"></div>
		</div>
	</div>
	<div class="col-md-9 col-sm-9 clear_both_1140">
		<div class="team_member_name">Roy Caniano</div>
		<div class="team_member_role">Vice President Consulting Services</div>
		<div class="see_at_all_res">
			<p class="general_paragraph">As the Vice President of Consulting Services for Conger-Elsea, Roy brings a unique combination of nuclear science expertise <span class="read_more_dots">...</span></p>
		</div>
		<a href="#roy_caniano_row" class="read_more_res">Read more</a>
		<div class="see_at_read_more"> 
			<p class="general_paragraph">and a global business strategy.  With more than 30 years of diversified experience as a regulator with the U.S. Nuclear Regulatory Commission (NRC), Roy is described as both an operation expert and a highly versatile corporate leader.  He is certified as a material and reactor radiation specialist and a materials licensing reviewer.<br /><br />As an operations expert, he has proficiencies and knowledge of the materials inspection and licensing program, the Reactor Oversight Process, the associated 10 CFR regulations and oversight of the agreement state program.  He is a proven leader of two high-visibility agency level augmented inspection teams associated with events in the nuclear power plant and materials programs.  And he has also served as a senior executive, managing a 70 member team with responsibilities to implement all inspection and licensing programs in the reactor and materials programs. With an extensive background as part of the USNRC enforcement and allegation process, Roy brings a wealth of knowledge and experiences to Conger-Elsea.</p>
			<a href="#roy_caniano_row_closed" class="read_less_res">Read less</a>
		</div>
	</div>
</div>
<div class="row team_member_row margin_top_50" id="mark_anderson_row">
	<div class="col-md-3 col-sm-3 clear_both_1140">
		<div class="team_outer_border">
			<div class="team_sprite mark_anderson"></div>
		</div>
	</div>
	<div class="col-md-9 col-sm-9 clear_both_1140">
		<div class="team_member_name">Mark Anderson</div>
		<div class="team_member_role">Managing Partner</div>
		<div class="see_at_all_res">
			<p class="general_paragraph">Mark is a relationship architect who brings his leadership talents, resources and entrepreneurial spirit to Conger-Elsea’s rich <span class="read_more_dots">...</span></p>
		</div>
		<a href="#mark_anderson_row" class="read_more_res">Read more</a>
		<div class="see_at_read_more"> 
			<p class="general_paragraph">history.  As a results-driven sales and marketing professional, he has led and consulted numerous Fortune 100 organizations to successfully develop Customer Relationship Management processes that deliver high-performance sales and service results.  To his credit, Mark was one of the team leads for the largest shift in marketplace momentum as a member of the Friends and Family sales and marketing team for MCI / Verizon.<br /><br />Then after 25 years, in 2006 Mark founded WinningOutlook, which provides consulting services for improving operations and processes, defining metrics and utilizing technology to effectively improve an enterprise brand, their sales, and their services.  Since joining the Conger-Elsea team, he adds marketing value and a performance record for translating that marketing value into results.  While bringing a wealth of talents to the Conger-Elsea executive team, Mark assists the team to mitigate client’s risk and solve/resolve problems that affect our client’s business.</p>
			<a href="#mark_anderson_row_closed" class="read_less_res">Read less</a>
		</div>
	</div>
</div>
<div class="row team_member_row margin_top_50" id="harry_fetterman_row">
	<div class="col-md-3 col-sm-3 clear_both_1140">
		<div class="team_outer_border">
			<div class="team_sprite harry_fetterman"></div>
		</div>
	</div>
	<div class="col-md-9 col-sm-9 clear_both_1140">
		<div class="team_member_name">Harry Fetterman</div>
		<div class="team_member_role">General Manager Training </div>
		<div class="see_at_all_res">
			<p class="general_paragraph">Serving as the General Manager of Training, Harry brings a rare combination of industry experiences, academic credentials and <span class="read_more_dots">...</span></p>
		</div>
		<a href="#harry_fetterman_row" class="read_more_res">Read more</a>
		<div class="see_at_read_more"> 
			<p class="general_paragraph">training expertise to the Conger-Elsea team.  He is a highly respected industry trainer and educator, with over 30 years experience in the nuclear industry, three of which include his concurrent affiliation with Thomas Edison State College in New Jersey.  Harry has managed the sophisticated organization that is required to maintain training and qualifications for a commercial nuclear power plant.  He has led high profile Root Cause Analyses involving Safety Culture, Technical Issues, Regulatory Issues and Inspections, and Training Program Deficiencies.  Harry has adapted and applied the rigorous techniques for evaluating accidents and incidents in several additional industries, such as pharmaceutical, oil and gas pipelines and transport of hazardous material.<br /><br />In 1996, Harry earned his PhD in Workforce Education and Development from Penn State University.  Harry is a professional trainer and training manager of diverse topics in the classroom and online settings.  Harry’s collective knowledge and skills promote the mission of the Conger-Elsea team, which is nationally and internationally recognized as a premier training and consulting organization.</p>
			<a href="#harry_fetterman_row_closed" class="read_less_res">Read less</a>
		</div>
	</div>
</div>
<hr class="custom_hr team">
<a href="#" class="about_team_letter">A LETTER TO OUR CLIENTS</a>
