<div class="slider bx-slider">
	<ul class="bxslider">
	  <li class="slide slide_1">
	    <img src="images/slider/slider_1.jpg" />
	  	<div class="slide_content">
	  		<div class="slide_text">Understanding Regulatory<br />Trends and Industry Issues</div>
	  		<div class="slide_link">
	  			<a href="#">LINK</a>
	  		</div>
	  	</div>
	  </li>
	  <li class="slide slide_2">
	  	<img src="images/slider/slider_2.jpg" />
	  	<div class="slide_content">
	  		<div class="slide_text">Understanding Regulatory<br />Trends and Industry Issues</div>
	  		<div class="slide_link">
	  			<a href="#">LINK</a>
	  		</div>
	  	</div>
	  </li>
	  <li class="slide slide_3">
	  	<img src="images/slider/slider_3.jpg" />
	  	<div class="slide_content">
	  		<div class="slide_text">Understanding Regulatory<br />Trends and Industry Issues</div>
	  		<div class="slide_link">
	  			<a href="#">LINK</a>
	  		</div>
	  	</div>
	  </li>
	</ul>
</div>